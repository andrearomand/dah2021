
part of mi_proyecto;
class CuartaPantalla extends StatefulWidget {
  const CuartaPantalla({Key? key}) : super(key: key);

  @override
  State<CuartaPantalla> createState() => _CuartaPantallaEstado();
}
class _CuartaPantallaEstado extends State<CuartaPantalla> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: InteractiveViewer(
         child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image (image: AssetImage('images/mapa_zoo.jpg')),
                        Text(
              '',
              style: Theme.of(context).textTheme.headline4,
            ),
            ElevatedButton(
            onPressed: () {
              Navigator.push(context, 
              MaterialPageRoute(builder: (context) => SegundaPantalla()));
            },
            child: const Text('Ir a Animales'),
          ),
          ],
        ),
      ),
    );
  }

}

