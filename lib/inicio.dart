part of mi_proyecto;

class PrimeraPantalla extends StatelessWidget {
  const PrimeraPantalla({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment:CrossAxisAlignment.center,
        children: <Widget>[
          Padding(padding: EdgeInsets.all(30.0)),
          Image (
          image: AssetImage('images/logocama.jpg')),
          const Text(
            ' Bienvenido/a ',
            style: TextStyle(
              color: Colors.blue,
              fontSize: 30.0,
            ),
          ),
          
           Padding(padding: EdgeInsets.all(30.0)),
            ElevatedButton(
            onPressed: () {},
             style: ElevatedButton.styleFrom(
                primary: Colors.teal),
            child: const Text('Iniciar sesión'),
          ),
          ElevatedButton(
            onPressed: () {},
             style: ElevatedButton.styleFrom(
                primary: Colors.purple),
            child: const Text('Iniciar sesión con Google'),
          ),
          ElevatedButton(
            onPressed: () {},
             style: ElevatedButton.styleFrom(
                primary: Colors.blue),
            child: const Text('Iniciar sesión con Facebook'),
          )
        ],
      ),
    );
  }
}
