part of mi_proyecto;
class SegundaPantalla extends StatefulWidget {
  const SegundaPantalla({Key? key}) : super(key: key);

  @override
  State<SegundaPantalla> createState() => _SegundaPantallaEstado();
}

class _SegundaPantallaEstado extends State<SegundaPantalla> {
   @override
    Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image(image: AssetImage('images/leopardo.jpg')),
              Padding(padding: EdgeInsets.all(5.0)),
              Text(
              'Leopardo (Phantera Pardus)',
              style: Theme.of(context).textTheme.headline5,
            ),
            Padding(padding: EdgeInsets.all(15.0),
              child: Text(
                'El leopardo es un mamífero carnívoro de la familia de los félidos. Al igual que tres de los demás félidos del género Panthera: el león, el tigre y el jaguar, están caracterizados por una modificación en el hueso hioides que les permite rugir',
              ),
            ),
            
            Padding(padding: EdgeInsets.all(20.0)),
            ElevatedButton(
            onPressed: () {
              Navigator.push(context, 
              MaterialPageRoute(builder: (context) => CuartaPantalla()));
            },
            style: ElevatedButton.styleFrom(
                primary: Colors.blue),
            child: const Text('Ver ubicación en el mapa'),
          ),
          ],
        ),
      ),
    );
  }
}
