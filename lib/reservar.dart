part of mi_proyecto;
class reservar extends StatefulWidget {
  const reservar({Key? key}) : super(key: key);

  @override
  State<reservar> createState() => _reservarEstado();
}
class _reservarEstado extends State<reservar> {
 @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
         child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Image (image: AssetImage('images/panda.png')),
                        Text(
              'Haz reservado tu visita <3 te esperamos!',
              style: Theme.of(context).textTheme.headline4,
            ),
            ElevatedButton(
            onPressed: () {
              Navigator.pop(context);
            },
            child: const Text('Volver'),
          ),
          ],
        ),
      ),
    );
  }

}